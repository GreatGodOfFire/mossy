macro_rules! op_code {
    ($( $variant:ident ),*) => {
        #[derive(Clone, Copy)]
        pub enum OpCode {
            $($variant),*
        }

        impl OpCode {
            pub fn name(&self) -> &'static str {
                match self {
                    $(
                        Self::$variant => stringify!($variant)
                     ),*
                }
            }
        }
    };
}

op_code! {
    BRK,
    ORA,
    ASL,
    PHP,
    BPL,
    CLC,
    JSR,
    AND,
    BIT,
    ROL,
    PLP,
    BMI,
    SEC,
    RTI,
    EOR,
    LSR,
    PHA,
    JMP,
    BVC,
    CLI,
    RTS,
    ADC,
    ROR,
    PLA,
    BVS,
    SEI,
    STA,
    STY,
    STX,
    DEY,
    TXA,
    BCC,
    TYA,
    TXS,
    LDY,
    LDA,
    LDX,
    TAY,
    TAX,
    BCS,
    CLV,
    TSX,
    CPY,
    CMP,
    DEC,
    INY,
    DEX,
    BNE,
    CLD,
    CPX,
    SBC,
    INC,
    INX,
    NOP,
    BEQ,
    SED,

    JAM,
    SLO,
    ANC,
    RLA,
    SRE,
    ALR,
    RRA,
    ARR,
    SAX,
    ANE,
    SHA,
    TAS,
    SHY,
    SHX,
    LAX,
    LXA,
    LAS,
    DCP,
    SBX,
    ISC,
    USBC
}
impl OpCode {
    pub fn is_legal(&self) -> bool {
        use OpCode::*;

        match self {
            JAM | SLO | ANC | RLA | SRE | ALR | RRA | ARR | SAX | ANE | SHA | TAS | SHY | SHX
            | LAX | LXA | LAS | DCP | SBX | ISC | USBC => false,
            _ => true,
        }
    }
}

#[derive(Clone, Copy)]
pub enum AddressMode {
    Accumulator,
    Absolute(u16),
    AbsoluteX(u16),
    AbsoluteY(u16),
    Immediate(u8),
    Implied,
    Indirect(u16),
    XIndirect(u8),
    IndirectY(u8),
    Relative(u8),
    Zeropage(u8),
    ZeropageX(u8),
    ZeropageY(u8),
}

#[derive(Clone)]
pub struct Instruction<'a> {
    op_code: OpCode,
    addr_mode: AddressMode,
    bytes: &'a [u8],
}

impl Instruction<'_> {
    pub fn op_code(&self) -> OpCode {
        self.op_code
    }

    pub fn addr_mode(&self) -> AddressMode {
        self.addr_mode
    }

    pub fn bytes(&self) -> &[u8] {
        self.bytes
    }
}

macro_rules! ins {
    ($op_code:ident, $addr_mode:ident) => {
        |bytes| {
            let (len, addr_mode) = $addr_mode(bytes)?;

            Some((&bytes[len as usize..], OpCode::$op_code, addr_mode, len))
        }
    };
}

#[rustfmt::skip]
const LOOKUP_TABLE: [fn(&[u8]) -> Option<(&[u8], OpCode, AddressMode, u8)>; 256] = [
    ins!(BRK, imp),   ins!(ORA, x_ind), ins!(JAM, imp),   ins!(SLO, x_ind),
    ins!(NOP, zpg),   ins!(ORA, zpg),   ins!(ASL, zpg),   ins!(SLO, zpg),
    ins!(PHP, imp),   ins!(ORA, imm),   ins!(ASL, acc),   ins!(ANC, imm),
    ins!(NOP, abs),   ins!(ORA, abs),   ins!(ASL, abs),   ins!(SLO, abs),

    ins!(BPL, rel),   ins!(ORA, ind_y), ins!(JAM, imp),   ins!(SLO, ind_y),
    ins!(NOP, zpg_x), ins!(ORA, zpg_x), ins!(ASL, zpg_x), ins!(SLO, zpg_x),
    ins!(CLC, imp),   ins!(ORA, abs_y), ins!(NOP, imp),   ins!(SLO, abs_y),
    ins!(NOP, abs_x), ins!(ORA, abs_x), ins!(ASL, abs_x), ins!(SLO, abs_x),

    ins!(JSR, abs),   ins!(AND, x_ind), ins!(JAM, imp),   ins!(RLA, x_ind),
    ins!(BIT, zpg),   ins!(AND, zpg),   ins!(ROL, zpg),   ins!(RLA, zpg),
    ins!(PLP, imp),   ins!(AND, imm),   ins!(ROL, acc),   ins!(ANC, imm),
    ins!(BIT, abs),   ins!(AND, abs),   ins!(ROL, abs),   ins!(RLA, abs),

    ins!(BMI, rel),   ins!(AND, ind_y), ins!(JAM, imp),   ins!(RLA, ind_y),
    ins!(NOP, zpg_x), ins!(AND, zpg_x), ins!(ROL, zpg_x), ins!(RLA, zpg_x),
    ins!(SEC, imp),   ins!(AND, abs_y), ins!(NOP, imp),   ins!(RLA, abs_y),
    ins!(NOP, abs_x), ins!(AND, abs_x), ins!(ROL, abs_x), ins!(RLA, abs_x),

    ins!(RTI, imp),   ins!(EOR, x_ind), ins!(JAM, imp),   ins!(SRE, x_ind),
    ins!(NOP, zpg),   ins!(EOR, zpg),   ins!(LSR, zpg),   ins!(SRE, zpg),
    ins!(PHA, imp),   ins!(EOR, imm),   ins!(LSR, acc),   ins!(ALR, imm),
    ins!(JMP, abs),   ins!(EOR, abs),   ins!(LSR, abs),   ins!(SRE, abs),

    ins!(BVC, rel),   ins!(EOR, ind_y), ins!(JAM, imp),   ins!(SRE, ind_y),
    ins!(NOP, zpg_x), ins!(EOR, zpg_x), ins!(LSR, zpg_x), ins!(SRE, zpg_x),
    ins!(CLI, imp),   ins!(EOR, abs_y), ins!(NOP, imp),   ins!(SRE, abs_y),
    ins!(NOP, abs_x), ins!(EOR, abs_x), ins!(LSR, abs_x), ins!(SRE, abs_x),

    ins!(RTS, imp),   ins!(ADC, x_ind), ins!(JAM, imp),   ins!(RRA, x_ind),
    ins!(NOP, zpg),   ins!(ADC, zpg),   ins!(ROR, zpg),   ins!(RRA, zpg),
    ins!(PLA, imp),   ins!(ADC, imm),   ins!(ROR, acc),   ins!(ARR, imm),
    ins!(JMP, ind),   ins!(ADC, abs),   ins!(ROR, abs),   ins!(RRA, abs),

    ins!(BVS, rel),   ins!(ADC, ind_y), ins!(JAM, imp),   ins!(RRA, ind_y),
    ins!(NOP, zpg_x), ins!(ADC, zpg_x), ins!(ROR, zpg_x), ins!(RRA, zpg_x),
    ins!(SEI, imp),   ins!(ADC, abs_y), ins!(NOP, imp),   ins!(RRA, abs_y),
    ins!(NOP, abs_x), ins!(ADC, abs_x), ins!(ROR, abs_x), ins!(RRA, abs_x),

    ins!(NOP, imm),   ins!(STA, x_ind), ins!(NOP, imm),   ins!(SAX, x_ind),
    ins!(STY, zpg),   ins!(STA, zpg),   ins!(STX, zpg),   ins!(SAX, zpg),
    ins!(DEY, imp),   ins!(NOP, imm),   ins!(TXA, imp),   ins!(ANE, imm),
    ins!(STY, abs),   ins!(STA, abs),   ins!(STX, abs),   ins!(SAX, abs),

    ins!(BCC, rel),   ins!(STA, ind_y), ins!(JAM, imp),   ins!(SHA, ind_y),
    ins!(STY, zpg_x), ins!(STA, zpg_x), ins!(STX, zpg_y), ins!(SAX, zpg_y),
    ins!(TYA, imp),   ins!(STA, abs_y), ins!(TXS, imp),   ins!(TAS, abs_y),
    ins!(SHY, abs_x), ins!(STA, abs_x), ins!(SHX, abs_y), ins!(SHA, abs_y),

    ins!(LDY, imm),   ins!(LDA, x_ind), ins!(LDX, imm),   ins!(LAX, x_ind),
    ins!(LDY, zpg),   ins!(LDA, zpg),   ins!(LDX, zpg),   ins!(LAX, zpg),
    ins!(TAY, imp),   ins!(LDA, imm),   ins!(TAX, imp),   ins!(LXA, imm),
    ins!(LDY, abs),   ins!(LDA, abs),   ins!(LDX, abs),   ins!(LAX, abs),

    ins!(BCS, rel),   ins!(LDA, ind_y), ins!(JAM, imp),   ins!(LAX, ind_y),
    ins!(LDY, zpg_x), ins!(LDA, zpg_x), ins!(LDX, zpg_y), ins!(LAX, zpg_y),
    ins!(CLV, imp),   ins!(LDA, abs_y), ins!(TSX, imp),   ins!(LAS, abs_y),
    ins!(LDY, abs_x), ins!(LDA, abs_x), ins!(LDX, abs_y), ins!(LAX, abs_y),

    ins!(CPY, imm),   ins!(CMP, x_ind), ins!(NOP, imm),   ins!(DCP, x_ind),
    ins!(CPY, zpg),   ins!(CMP, zpg),   ins!(DEC, zpg),   ins!(DCP, zpg),
    ins!(INY, imp),   ins!(CMP, imm),   ins!(DEX, imp),   ins!(SBX, imm),
    ins!(CPY, abs),   ins!(CMP, abs),   ins!(DEC, abs),   ins!(DCP, abs),

    ins!(BNE, rel),   ins!(CMP, ind_y), ins!(JAM, imp),   ins!(DCP, ind_y),
    ins!(NOP, zpg_x), ins!(CMP, zpg_x), ins!(DEC, zpg_x), ins!(DCP, zpg_x),
    ins!(CLD, imp),   ins!(CMP, abs_y), ins!(NOP, imp),   ins!(DCP, abs_y),
    ins!(NOP, abs_x), ins!(CMP, abs_x), ins!(DEC, abs_x), ins!(DCP, abs_x),

    ins!(CPX, imm),   ins!(SBC, x_ind), ins!(NOP, imm),   ins!(ISC, x_ind),
    ins!(CPX, zpg),   ins!(SBC, zpg),   ins!(INC, zpg),   ins!(ISC, zpg),
    ins!(INX, imp),   ins!(SBC, imm),   ins!(NOP, imp),   ins!(USBC, imm),
    ins!(CPX, abs),   ins!(SBC, abs),   ins!(INC, abs),   ins!(ISC, abs),

    ins!(BEQ, rel),   ins!(SBC, ind_y), ins!(JAM, imp),   ins!(ISC, ind_y),
    ins!(NOP, zpg_x), ins!(SBC, zpg_x), ins!(INC, zpg_x), ins!(ISC, zpg_x),
    ins!(SED, imp),   ins!(SBC, abs_y), ins!(NOP, imp),   ins!(ISC, abs_y),
    ins!(NOP, abs_x), ins!(SBC, abs_x), ins!(INC, abs_x), ins!(ISC, abs_x),
    ];

pub fn parse_instrs(buf: &[u8]) -> (&[u8], Vec<Instruction>) {
    let mut instrs = vec![];

    let mut buf = buf;

    while let Some((data, instr)) = parse_instr(&mut buf) {
        instrs.push(instr);

        buf = data;
    }

    (buf, instrs)
}

pub fn parse_instr(buf: &[u8]) -> Option<(&[u8], Instruction)> {
    if buf.is_empty() {
        return None;
    }

    let op_code = buf[0];

    let (data, op_code, addr_mode, len) = LOOKUP_TABLE[op_code as usize](&buf[1..])?;

    Some((
        data,
        Instruction {
            op_code,
            addr_mode,
            bytes: &buf[..len as usize + 1],
        },
    ))
}

#[inline]
fn acc(_: &[u8]) -> Option<(u8, AddressMode)> {
    Some((0, AddressMode::Accumulator))
}

#[inline]
fn abs(buf: &[u8]) -> Option<(u8, AddressMode)> {
    if buf.len() < 2 {
        return None;
    }

    Some((
        2,
        AddressMode::Absolute(u16::from_le_bytes(buf[0..2].try_into().unwrap())),
    ))
}

#[inline]
fn abs_x(buf: &[u8]) -> Option<(u8, AddressMode)> {
    if buf.len() < 2 {
        return None;
    }

    Some((
        2,
        AddressMode::AbsoluteX(u16::from_le_bytes(buf[0..2].try_into().unwrap())),
    ))
}

#[inline]
fn abs_y(buf: &[u8]) -> Option<(u8, AddressMode)> {
    if buf.len() < 2 {
        return None;
    }

    Some((
        2,
        AddressMode::AbsoluteY(u16::from_le_bytes(buf[0..2].try_into().unwrap())),
    ))
}

#[inline]
fn imm(buf: &[u8]) -> Option<(u8, AddressMode)> {
    if buf.is_empty() {
        return None;
    }

    Some((1, AddressMode::Immediate(buf[0])))
}

#[inline]
fn imp(_: &[u8]) -> Option<(u8, AddressMode)> {
    Some((0, AddressMode::Implied))
}

#[inline]
fn ind(buf: &[u8]) -> Option<(u8, AddressMode)> {
    if buf.len() < 2 {
        return None;
    }

    Some((
        2,
        AddressMode::Indirect(u16::from_le_bytes(buf[0..2].try_into().unwrap())),
    ))
}

#[inline]
fn x_ind(buf: &[u8]) -> Option<(u8, AddressMode)> {
    if buf.is_empty() {
        return None;
    }

    Some((1, AddressMode::XIndirect(buf[0])))
}

#[inline]
fn ind_y(buf: &[u8]) -> Option<(u8, AddressMode)> {
    if buf.is_empty() {
        return None;
    }

    Some((1, AddressMode::IndirectY(buf[0])))
}

#[inline]
fn rel(buf: &[u8]) -> Option<(u8, AddressMode)> {
    if buf.is_empty() {
        return None;
    }

    Some((1, AddressMode::Relative(buf[0])))
}

#[inline]
fn zpg(buf: &[u8]) -> Option<(u8, AddressMode)> {
    if buf.is_empty() {
        return None;
    }

    Some((1, AddressMode::Relative(buf[0])))
}

#[inline]
fn zpg_x(buf: &[u8]) -> Option<(u8, AddressMode)> {
    if buf.is_empty() {
        return None;
    }

    Some((1, AddressMode::Relative(buf[0])))
}

#[inline]
fn zpg_y(buf: &[u8]) -> Option<(u8, AddressMode)> {
    if buf.is_empty() {
        return None;
    }

    Some((1, AddressMode::Relative(buf[0])))
}

#[test]
fn parse_test() {
    #[rustfmt::skip]
    let instrs = [
        0,
        1, 0,
        2,
        3, 0,
        4, 0,
        5, 0,
        6, 0,
        7, 0,
        8,
        9, 0,
        10,
        11, 0,
        12, 0, 0,
        13, 0, 0,
        14, 0, 0,
        15, 0, 0,
        16, 0,
        17, 0,
        18,
        19, 0,
        20, 0,
        21, 0,
        22, 0,
        23, 0,
        24,
        25, 0, 0,
        26,
        27, 0, 0,
        28, 0, 0,
        29, 0, 0,
        30, 0, 0,
        31, 0, 0,
        32, 0, 0,
        33, 0,
        34,
        35, 0,
        36, 0,
        37, 0,
        38, 0,
        39, 0,
        40,
        41, 0,
        42,
        43, 0,
        44, 0, 0,
        45, 0, 0,
        46, 0, 0,
        47, 0, 0,
        48, 0,
        49, 0,
        50,
        51, 0,
        52, 0,
        53, 0,
        54, 0,
        55, 0,
        56,
        57, 0, 0,
        58,
        59, 0, 0,
        60, 0, 0,
        61, 0, 0,
        62, 0, 0,
        63, 0, 0,
        64,
        65, 0,
        66,
        67, 0,
        68, 0,
        69, 0,
        70, 0,
        71, 0,
        72,
        73, 0,
        74,
        75, 0,
        76, 0, 0,
        77, 0, 0,
        78, 0, 0,
        79, 0, 0,
        80, 0,
        81, 0,
        82,
        83, 0,
        84, 0,
        85, 0,
        86, 0,
        87, 0,
        88,
        89, 0, 0,
        90,
        91, 0, 0,
        92, 0, 0,
        93, 0, 0,
        94, 0, 0,
        95, 0, 0,
        96,
        97, 0,
        98,
        99, 0,
        100, 0,
        101, 0,
        102, 0,
        103, 0,
        104,
        105, 0,
        106,
        107, 0,
        108, 0, 0,
        109, 0, 0,
        110, 0, 0,
        111, 0, 0,
        112, 0,
        113, 0,
        114,
        115, 0,
        116, 0,
        117, 0,
        118, 0,
        119, 0,
        120,
        121, 0, 0,
        122,
        123, 0, 0,
        124, 0, 0,
        125, 0, 0,
        126, 0, 0,
        127, 0, 0,
        128, 0,
        129, 0,
        130, 0,
        131, 0,
        132, 0,
        133, 0,
        134, 0,
        135, 0,
        136,
        137, 0,
        138,
        139, 0,
        140, 0, 0,
        141, 0, 0,
        142, 0, 0,
        143, 0, 0,
        144, 0,
        145, 0,
        146,
        147, 0,
        148, 0,
        149, 0,
        150, 0,
        151, 0,
        152,
        153, 0, 0,
        154,
        155, 0, 0,
        156, 0, 0,
        157, 0, 0,
        158, 0, 0,
        159, 0, 0,
        160, 0,
        161, 0,
        162, 0,
        163, 0,
        164, 0,
        165, 0,
        166, 0,
        167, 0,
        168,
        169, 0,
        170,
        171, 0,
        172, 0, 0,
        173, 0, 0,
        174, 0, 0,
        175, 0, 0,
        176, 0,
        177, 0,
        178,
        179, 0,
        180, 0,
        181, 0,
        182, 0,
        183, 0,
        184,
        185, 0, 0,
        186,
        187, 0, 0,
        188, 0, 0,
        189, 0, 0,
        190, 0, 0,
        191, 0, 0,
        192, 0,
        193, 0,
        194, 0,
        195, 0,
        196, 0,
        197, 0,
        198, 0,
        199, 0,
        200,
        201, 0,
        202,
        203, 0,
        204, 0, 0,
        205, 0, 0,
        206, 0, 0,
        207, 0, 0,
        208, 0,
        209, 0,
        210,
        211, 0,
        212, 0,
        213, 0,
        214, 0,
        215, 0,
        216,
        217, 0, 0,
        218,
        219, 0, 0,
        220, 0, 0,
        221, 0, 0,
        222, 0, 0,
        223, 0, 0,
        224, 0,
        225, 0,
        226, 0,
        227, 0,
        228, 0,
        229, 0,
        230, 0,
        231, 0,
        232,
        233, 0,
        234,
        235, 0,
        236, 0, 0,
        237, 0, 0,
        238, 0, 0,
        239, 0, 0,
        240, 0,
        241, 0,
        242,
        243, 0,
        244, 0,
        245, 0,
        246, 0,
        247, 0,
        248,
        249, 0, 0,
        250,
        251, 0, 0,
        252, 0, 0,
        253, 0, 0,
        254, 0, 0,
        255, 0, 0,
        ].as_slice();

    let _instrs = parse_instrs(instrs);
}
