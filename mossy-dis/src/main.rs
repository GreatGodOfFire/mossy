use std::{env::args, fs::File, io::Read};

use mossy_core::{parse_instrs, AddressMode};

fn main() {
    if let Some(file_name) = args().nth(1) {
        let mut file = File::open(file_name).unwrap();

        let mut buf = Vec::new();

        file.read_to_end(&mut buf).unwrap();

        let (_, instrs) = parse_instrs(&buf);

        for instr in instrs {
            println!(
                "{}{}  {} {}",
                instr
                    .bytes()
                    .iter()
                    .map(|b| format!("{:02x}", b))
                    .collect::<Vec<String>>()
                    .join(" "),
                if instr.bytes().len() < 3 { "\t" } else { "" },
                instr.op_code().name(),
                format_addr_mode(instr.addr_mode())
            );
        }
    } else {
        todo!()
    }
}

fn format_addr_mode(addr_mode: AddressMode) -> String {
    use AddressMode::*;

    match addr_mode {
        Accumulator => "A".to_string(),
        Absolute(x) => format!("${:04x}", x),
        AbsoluteX(x) => format!("${:04x},x", x),
        AbsoluteY(x) => format!("${:04x},y", x),
        Immediate(x) => format!("#${:02x}", x),
        Implied => "".to_string(),
        Indirect(x) => format!("(${:04x})", x),
        XIndirect(x) => format!("(${:02x},x)", x),
        IndirectY(x) => format!("(${:02x}),y", x),
        Relative(x) => format!("${:02x}", x),
        Zeropage(x) => format!("${:02x}", x),
        ZeropageX(x) => format!("${:02x},x", x),
        ZeropageY(x) => format!("${:02x},y", x),
    }
}
